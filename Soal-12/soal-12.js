//Jawaban Soal 12 Quiz
class BangunDatar {
    constructor(nama) {
        this._namabangundatar = nama;
    }

    luas() {
        return "";
    }

    keliling() {
        return "";
    }

    get nama(){
        return this._namabangundatar;
    }

    set nama(x){
        this._namabangundatar = x;
    }
}

var bangunan = new BangunDatar("Bangun Datar");

console.log(bangunan.nama);
bangunan.luas();
bangunan.keliling();

//Class Lingkaran
class Lingkaran extends BangunDatar {
    constructor(nama) {
        super(nama);
    }

    luasLingkaran(jariJari) {
        var pi = jariJari % 7 === 0 ? 22/7 : 3.14;
        var hasil = pi*jariJari*jariJari;
        console.log("Luas Lingkaran adalah " + hasil);
    }
    
    kelilingLingkaran(jariJari) {
        var pi = jariJari % 7 === 0 ? 22/7 : 3.14;
        var hasil = 2*(pi*jariJari*jariJari);
        console.log("Keliling Lingkaran adalah " + hasil);
    }
}

class Persegi extends BangunDatar {
    constructor(nama) {
        super(nama);
    }

    luasPersegi(sisi) {
        var hasil = sisi*sisi;
        console.log("Luas Persegi adalah " + hasil);
    }
    
    kelilingPersegi(sisi) {
        var hasil = 4*sisi;
        console.log("Keliling Persegi adalah " + hasil);
    }
}

var BangunanLingkaran = new Lingkaran("Lingkaran");

console.log("Nama Bangun Datar : " + BangunanLingkaran.nama);
BangunanLingkaran.luasLingkaran(7);
BangunanLingkaran.kelilingLingkaran(7);

var BangunanPersegi = new Persegi("Persegi");

console.log("Nama Bangun Datar : " + BangunanPersegi.nama);
BangunanPersegi.luasPersegi(4);
BangunanPersegi.kelilingPersegi(4);