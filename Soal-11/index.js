//Jawaban Soal 11 Quiz Memanggil Promise
var filterBookPromise = require('./promise.js')

var colorful = false;
var amountOfPage = 260;

function execute(colorful, amountOfPage){
    filterBookPromise(colorful, amountOfPage)
    .then(function (fulfilled){
        console.log(fulfilled);
    })
    .catch(function (error) {
        console.log(error.message);
    });
};

execute(colorful, amountOfPage);