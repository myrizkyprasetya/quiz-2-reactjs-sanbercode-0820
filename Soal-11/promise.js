// Soal 11 Quiz
function filterBookPromise(colorful, amountOfPage){
    return new Promise(function(resolve, reject){
        var books = [
            {name: "Shinchan", totalPage: 50, isColorful: true},
            {name: "Kalkulus", totalPage: 250, isColorful: false},
            {name: "Doraemon", totalPage: 40, isColorful: true},
            {name: "Algoritma", totalPage: 300, isColorful: false}
        ]
        if (amountOfPage > 0){
            resolve(books.filter(x=> x.totalPage >= amountOfPage && x.isColorful == colorful));
        } else{
            var reason = new Error("maaf parameter salah")
            reject(reason);
        }
    });
}

module.exports = filterBookPromise;